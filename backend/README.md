# Backend

Serverless API Backend managed by AWS Sam

## Requirements

- [AWS CLI](https://aws.amazon.com/cli/)
- [AWS SAM](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-install.html)
- [NodeJS 10.16.3+](https://nodejs.org/en/download/)
- [Yarn](https://yarnpkg.com/lang/en/docs/install/)
- [Docker (Only if you want to run lambda locally via SAM)](https://www.docker.com/community-edition)

## Setup process

### Installing dependencies

We use `yarn` to manage NodeJS dependencies:

```bash
yarn install
```

### Local development

The typescript compiler will watch the src directory for changes and restart the local server.

NOTE: Make sure to have proper [AWS credentials](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html) in your environment.

```bash
yarn dev
```

We recommend installing the `pino-pretty` package globally for better log output.

```bash
yarn global add pino-pretty
```

```bash
yarn dev | pino-pretty
```

### Simulating the AWS Lambda environment

We use `Docker` and `AWS SAM` to spin up a local server that simulates an AWS Lambda environment
and allows you to invoke the function locally.

```bash
yarn sam:local
```

## Packaging and deployment

We use `AWS SAM` to deploy our code to `AWS`.

The following command will do the following:

- Build the project.
- Package the compiled code into a zip file.
- Upload the zip file to AWS S3.
- Create/update and wire up a lambda function with the uploaded code.
- Expose and endpoint via `AWS API Gateway` that invokes our function when called.

```bash
yarn deploy
```
