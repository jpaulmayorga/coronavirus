import * as Emittery from 'emittery';
import { FastifyInstance } from 'fastify';
import * as plugin from 'fastify-plugin';

export const emitter = plugin.default(async function emitterPlugin(server: FastifyInstance) {
  const emitterInstance = new Emittery.default();
  server.decorate('emitter', emitterInstance);
  server.decorateRequest('emitter', emitterInstance);
});

declare module 'fastify' {
  interface FastifyInstance {
    emitter: Emittery;
  }

  interface FastifyRequest {
    emitter: Emittery;
  }
}
