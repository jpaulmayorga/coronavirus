import { FastifyInstance } from 'fastify';
import * as plugin from 'fastify-plugin';
import { Logger } from 'pino';

import { FirebaseApi } from '../lib/FirebaseApi';

export const firebase = plugin.default(async function firebasePlugin(
  server: FastifyInstance,
  options: { databaseUrl: string; projectId: string; clientEmail: string; privateKey: string },
) {
  const fb = new FirebaseApi({
    logger: server.log as Logger,
    databaseUrl: options.databaseUrl,
    projectId: options.projectId,
    clientEmail: options.clientEmail,
    privateKey: options.privateKey,
  });
  server.decorateRequest('firebase', fb);
});

declare module 'fastify' {
  interface FastifyRequest {
    firebase: FirebaseApi;
  }
}
