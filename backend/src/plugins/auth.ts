import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import * as plugin from 'fastify-plugin';
import * as FirebaseAdmin from 'firebase-admin';
import { ServerResponse } from 'http';
// import { Logger } from 'pino';

// import { UsersRepository } from '../api/users/Repository';
// import { config } from '../config';

export const auth = plugin.default(async function authPlugin(server: FastifyInstance) {
  server.decorate('authFirebaseCookie', authFirebaseCookie);
  server.decorate('authFirebaseBodyToken', authFirebaseBodyToken);
  server.decorateReply('addSessionCookie', addSessionCookie);
});

type AuthFirebaseTokenRequest = FastifyRequest & {
  body: { token: string };
};

/**
 * A middleware function that looks for a [ID token](https://firebase.google.com/docs/auth/admin/verify-id-tokens) in
 * a request's body and verifies it. The decoded token claims are stored in req.credentials.
 * Useful for signup endpoints.
 */
async function authFirebaseBodyToken(
  req: AuthFirebaseTokenRequest,
  _reply: FastifyReply<ServerResponse>,
) {
  const token = req.body.token;
  const decoded = await req.firebase.verifyIdToken({ token });
  req.credentials = decoded;
}

/**
 * A middleware function that authenticates a
 * [firebase session cookie](https://firebase.google.com/docs/auth/admin/manage-cookies)
 *
 */
async function authFirebaseCookie(req: FastifyRequest, _reply: FastifyReply<ServerResponse>) {
  const decoded = await req.firebase.verifySessionCookie({ cookie: req.cookies.session || '' });
  req.credentials = decoded;
  // const usersRepository = new UsersRepository({
  //   logger: req.log as Logger,
  //   tableName: config.X_USERS_TABLE,
  // });
  // const user = await usersRepository.getOne({ userId: decoded.uid });
  req.credentials = { ...decoded };
  // req.credentials = { ...decoded, ...user };
}

/**
 * A reply decorator function that adds a session cookie to the reply given a token
 */
async function addSessionCookie(this: FastifyReply<ServerResponse>, token: string) {
  const twoWeeks = 1000 * 60 * 60 * 24 * 14;
  const cookie = await this.request.firebase.createSessionCookie({
    token,
    expiresInMillis: twoWeeks,
  });
  this.setCookie('session', cookie, {
    maxAge: twoWeeks,
    httpOnly: true,
    path: '/',
  });
}

declare module 'fastify' {
  interface FastifyInstance {
    authFirebaseBodyToken: any;
    authFirebaseCookie: any;
  }

  interface FastifyRequest {
    credentials?: FirebaseAdmin.auth.DecodedIdToken & { [key: string]: any };
  }

  interface FastifyReply<HttpResponse> {
    addSessionCookie: (token: string) => Promise<void>;
  }
}
