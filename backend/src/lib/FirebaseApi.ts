import * as FirebaseAdmin from 'firebase-admin';
import { Logger } from 'pino';

export class FirebaseApi {
  static credentialFromServiceAccount(serviceAccount: FirebaseAdmin.ServiceAccount) {
    return FirebaseAdmin.credential.cert(serviceAccount);
  }

  logger: Logger;
  firebase: FirebaseAdmin.app.App;

  constructor(options: {
    logger: Logger;
    databaseUrl: string;
    privateKey: string;
    projectId: string;
    clientEmail: string;
  }) {
    this.logger = options.logger;
    this.firebase = FirebaseAdmin.initializeApp({
      databaseURL: options.databaseUrl,
      credential: FirebaseAdmin.credential.cert({
        privateKey: options.privateKey,
        projectId: options.projectId,
        clientEmail: options.clientEmail,
      }),
    });
  }

  async createSessionCookie(options: { token: string; expiresInMillis: number }) {
    return this.firebase
      .auth()
      .createSessionCookie(options.token, { expiresIn: options.expiresInMillis });
  }

  async setCustomClaims(options: { userId: string; claims: any }) {
    return this.firebase.auth().setCustomUserClaims(options.userId, options.claims);
  }

  async verifySessionCookie(options: { cookie: string }) {
    return this.firebase.auth().verifySessionCookie(options.cookie, true);
  }

  async verifyIdToken(options: { token: string }) {
    return this.firebase.auth().verifyIdToken(options.token);
  }
}
