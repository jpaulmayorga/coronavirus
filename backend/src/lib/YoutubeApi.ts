import { google, GoogleApis, youtube_v3 } from 'googleapis';
import { Logger } from 'pino';

export class YoutubeApi {
  google: GoogleApis;
  logger: Logger;
  youtube: youtube_v3.Youtube;

  constructor(options: { logger: Logger; accessToken: string; google?: GoogleApis }) {
    this.logger = options.logger;
    this.google = options.google || google;
    const oAuth2Client = new this.google.auth.OAuth2();
    oAuth2Client.setCredentials({ access_token: options.accessToken });
    this.youtube = this.google.youtube({ version: 'v3', auth: oAuth2Client });
  }

  async getChannelId() {
    const result = await this.youtube.channels.list({ part: 'id', mine: true });
    return result.data.items![0].id as string;
  }
}
