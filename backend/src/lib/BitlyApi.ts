import * as axios from 'axios';
import { Logger } from 'pino';

export interface Bitlink {
  domain: string;
  title?: string;
  group_guid: string;
  long_url: string;
}

export interface CustomBitlink {
  bitlink_id: string;
  custom_bitlink: string;
}

export class BitLinkApi {
  logger: Logger;
  private client: axios.AxiosInstance;

  constructor(options: { logger: Logger; bitlyToken: string }) {
    this.logger = options.logger;
    this.client = axios.default.create({
      baseURL: `https://api-ssl.bitly.com/v4`,
      headers: {
        'Content-Type': 'application/json',
        // tslint:disable-next-line: object-literal-key-quotes
        Authorization: `Bearer ${options.bitlyToken}`,
      },
    });
  }

  async bitlinks(bitlink: Bitlink) {
    const response = await this.client.post('/bitlinks', bitlink);
    return response.data;
  }

  async customBitLinks(customBitLinks: CustomBitlink) {
    const response = await this.client.post('/custom_bitlinks', customBitLinks);
    return response.data;
  }
}
