import aws from 'aws-sdk';
import fastify from 'fastify';
import fastifyAuth from 'fastify-auth';
import fastifyCookie from 'fastify-cookie';
import fastifyCors from 'fastify-cors';
import fastifyCsrf from 'fastify-csrf';

import { casesApi } from './api/cases';
import { config } from './config';
import { logRequestHook } from './hooks/log-request';
import { logResponseHook } from './hooks/log-response';
import { auth } from './plugins/auth';
import { emitter } from './plugins/emitter';
import { firebase } from './plugins/firebase';

export function init() {
  aws.config.update({ region: 'us-east-1' });
  const app = fastify({ logger: true, disableRequestLogging: true });
  app.register(emitter);
  app.register(fastifyCors, {
    origin: [
      'https://storeapp.tus10segundos.com',
      'https://support.tus10segundos.com',
      'https://creadores.tus10segundos.com',
      'http://localhost:8080',
      'http://localhost:9090',
      'http://localhost:3000',
    ],
    allowedHeaders: [
      'Origin',
      'Referer',
      'User-Agent',
      'Authorization',
      'Content-Type',
      'Content-Language',
      'Accept',
      'Accept-Language',
      'Accept-Encoding',
      'X-Csrf-Token',
    ],
    credentials: true,
  });
  app.register(fastifyCookie);
  app.register(fastifyCsrf, {
    key: '_csrf',
    cookie: true,
    ignoreMethods: ['GET', 'HEAD', 'OPTIONS'],
  });
  app.register(fastifyAuth);

  app.addHook('onRequest', logRequestHook);
  app.addHook('onResponse', logResponseHook);
  app.register(firebase, {
    projectId: config.X_FIREBASE_PROJECT_ID,
    clientEmail: config.X_FIREBASE_CLIENT_EMAIL,
    privateKey: JSON.parse(`"${config.X_FIREBASE_PRIVATE_KEY}"`),
    databaseUrl: config.X_FIREBASE_DB_URL,
  });
  app.register(auth);

  app.register(casesApi);

  return app;
}

if (require.main === module) {
  // called directly i.e. "node app"
  init().listen(3000, (err: Error) => {
    if (err) {
      // tslint:disable-next-line: no-console
      console.error(err);
    }
    // tslint:disable-next-line: no-console
    console.log('server listening on 3000');
  });
} else {
  // required as a module => executed on aws lambda
}
