import * as envalid from 'envalid';
import path from 'path';

const { str } = envalid;

export const config = envalid.cleanEnv(
  process.env,
  {
    X_FIREBASE_DB_URL: str(),
    X_FIREBASE_PROJECT_ID: str(),
    X_FIREBASE_CLIENT_EMAIL: str(),
    X_FIREBASE_PRIVATE_KEY: str(),
    X_CASES_TABLE: str(),
  },
  { strict: true, dotEnvPath: path.resolve(__dirname, '../.env') },
);
