import { DocumentClient } from 'aws-sdk/clients/dynamodb';
import CreateError from 'http-errors';
import { Logger } from 'pino';

export interface CaseType {
  caseId: string;
  email: string;
  emailVerified: boolean;
  namePretty: string;
  name: string;
  photoUrl: string;
  status: string;
  createdAt: string;
  admin?: boolean;
}

export class CasesRepository {
  logger: Logger;
  tableName: string;
  private db: DocumentClient;

  constructor(options: { logger: Logger; tableName: string; db?: DocumentClient }) {
    this.logger = options.logger;
    this.db = options.db || new DocumentClient();
    this.tableName = options.tableName;
  }

  async create(caseItem: CaseType) {
    let query;
    try {
      query = await this.db
        .put({
          TableName: this.tableName,
          Item: caseItem,
          ConditionExpression: 'attribute_not_exists(caseId)',
        })
        .promise();
    } catch (err) {
      if (err.code === 'ConditionalCheckFailedException') {
        throw CreateError(409, 'case already exists.');
      }
      throw err;
    }

    return query.Attributes;
  }

  async getOne(options: { userId: string }) {
    const query = await this.db
      .get({
        TableName: this.tableName,
        Key: {
          userId: options.userId,
        },
      })
      .promise();

    if (query.Item == null) {
      throw CreateError(404, 'User not found.');
    }

    return query.Item;
  }

  async getMany() {
    const query = await this.db
      .scan({
        TableName: this.tableName,
      })
      .promise();
    return query.Items;
  }
}
