import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { ServerResponse } from 'http';
import CreateError from 'http-errors';
// import { Logger } from 'pino';

// import { config } from '../../config';

// import { CasesRepository } from './Repository';

export async function casesApi(server: FastifyInstance) {
  server.route({
    method: 'GET',
    url: '/works',
    preHandler: [server.auth([server.authFirebaseCookie])],
    handler: async (req: FastifyRequest, reply: FastifyReply<ServerResponse>) => {
      const credentials = req.credentials;
      if (!credentials) {
        throw CreateError(401, `Unauthorized request`);
      }

      // const casesRepository = new CasesRepository({
      //   logger: req.log as Logger,
      //   tableName: config.X_CASES_TABLE,
      // });
      // const data = await casesRepository.getOne({ userId: credentials.uid });

      reply.code(200).send({ data: { works: true } });
    },
  });
}
